﻿using Game = GameOfBattleships.Core.GameOfBattleships;
using System;

namespace GameOfBattleships.Domain
{
    class Program
    {
        static void Main()
        {
            Console.CursorVisible = false;

            do
            {
                var game = new Game();

                UI.Play(game);
            } while (UI.Prompt("Play again? (y/n) ", ConsoleKey.Y, ConsoleKey.N));

            Console.ReadKey();
        }
    }
}
