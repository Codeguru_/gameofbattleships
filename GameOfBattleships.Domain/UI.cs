﻿using GameOfBattleships.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Game = GameOfBattleships.Core.GameOfBattleships;

namespace GameOfBattleships.Domain
{
    static class UI
    {
        static readonly Dictionary<char, int> lettersIndexes = new Dictionary<char, int>
        {
            { 'A',  1 },
            { 'B',  2 },
            { 'C',  3 },
            { 'D',  4 },
            { 'E',  5 },
            { 'F',  6 },
            { 'G',  7 },
            { 'H',  8 },
            { 'I',  9 },
            { 'J',  10 }
        };

        internal const byte offset = 4;

        internal static void Play(Game game)
        {
            game.StartNewGame(vsAIPlayer: true);

            // start of game

            while (game.PlayerToMove.availableShips.Values.Any(v => v > 0))
            {
                UI.UpdateMaps(game);

                var availableShips = game.PlayerToMove.availableShips;

                var currentShipInfo = availableShips.FirstOrDefault(s => s.Value > 0);

                UI.PickShipStartPositions(game, currentShipInfo.Key);

                Console.Clear();
            }

            UI.UpdateMaps(game);

            // gameplay

            bool? pl1won = null;

            while (pl1won == null)
            {
                UI.UpdateMaps(game);

                UI.ShootOnTarget(game);

                if (game.Player2.FleetIsDestroyed)
                {
                    pl1won = true;
                }

                if (game.Player1.FleetIsDestroyed)
                {
                    pl1won = false;
                }

                Console.Clear();
            }

            Console.WriteLine("{0} has just won the game! Congratulations!", 
                pl1won.Value ? 
                game.Player1.name : 
                game.Player2.name);            
        }

        internal static bool Prompt(string message, ConsoleKey yes, ConsoleKey no)
        {
            Console.Write(message);

            ConsoleKey key = default(ConsoleKey);

            while (key != yes && key != no)
            {
                key = Console.ReadKey(true).Key;
            }

            Console.WriteLine(Environment.NewLine);

            return key == yes;
        }

        internal static void ShootOnTarget(Game game)
        {
            var rand = new Random();
            var operationIsOk = true;
            var isHit = false;
            int[] xy;

            do
            {
                Console.CursorTop = 18;

                if (!operationIsOk)
                {
                    Console.Write("\n");
                    Console.CursorLeft = offset;
                    Console.WriteLine("Wrong coordinates! Please try again!");
                    Thread.Sleep(1500);
                    Console.CursorTop -= 3;
                    Console.WriteLine(new string(' ', 70));
                    Console.WriteLine(new string(' ', 70));
                    Console.WriteLine(new string(' ', 70));
                    Console.WriteLine(new string(' ', 70));
                    Console.WriteLine(new string(' ', 70));
                    Console.CursorTop -= 3;
                    operationIsOk = true;
                }

                if (!game.Pl1ToMove && game.VsAIPlayer)
                {
                    Position newPosition = game.AiPlayer.MakeTurn();

                    isHit = game.Shoot(newPosition);

                    Console.CursorLeft = offset;

                    Console.WriteLine("Opponent has fired on square {0}{1} and {2}",
                                        lettersIndexes.First(l => l.Value == newPosition.X).Key, newPosition.Y, isHit ? 
                                          game.Opponent.IsShipDestroyed(newPosition) ?
                                            "destroyed your " + game.Opponent.latestDestroyedShip.name :
                                              "hit the target!" : "missed!");                    

                    Thread.Sleep(2500);

                    break;
                }

                Console.CursorLeft = offset;

                Console.Write("Please select square to fire : ");

                string input = Console.ReadLine().ToUpper();
                char xChar = input[0];

                try
                {
                    xy = new[]
                    {
                        lettersIndexes[xChar],
                        Convert.ToInt32(input.Substring(1, input.Length - 1))
                    };                    
                }
                catch
                {
                    operationIsOk = false;
                    continue;
                }

                if (xy[0] < 1 || xy[0] > game.gameFieldSize ||
                    xy[1] < 1 || xy[1] > game.gameFieldSize)
                {
                    operationIsOk = false;
                    continue;
                }

                Position pos = Position.Get(xy[0], xy[1]);

                isHit = game.Shoot(pos);

                Console.CursorLeft = offset;

                Console.WriteLine("You've fired on square {0}{1} and {2}",
                    xChar, xy[1], isHit ? game.Opponent.IsShipDestroyed(pos) ? 
                      "destroyed opponent's " + game.Opponent.latestDestroyedShip.name :
                        "hit the target!" : "missed!");

                Thread.Sleep(2500);

            } while (!operationIsOk);
        }

        internal static void PickShipStartPositions(Game game, Type currentShipType)
        {
            int[] coordinates = null;
            Alignment alignment;
            bool operationIsOk = true;

            do
            {
                if (!operationIsOk)
                {
                    Console.Write("\n");
                    Console.CursorLeft = offset;
                    Console.WriteLine("Wrong coordinates! Please try again!");
                    Thread.Sleep(1500);
                    Console.CursorTop -= 3;
                    Console.WriteLine(new string(' ', 70));
                    Console.WriteLine(new string(' ', 70));
                    Console.WriteLine(new string(' ', 70));
                    Console.WriteLine(new string(' ', 70));
                    Console.WriteLine(new string(' ', 70));
                    Console.CursorTop -= 3;
                    operationIsOk = true;
                }

                if (!game.Pl1ToMove && game.VsAIPlayer)
                {
                    game.AddShipToRandomPos(currentShipType);
                    break;
                }

                Console.CursorLeft = offset;
                Console.CursorTop = 18;

                Console.Write("Please select where to place your {0} (A1) / random : ", currentShipType.Name);

                string input = Console.ReadLine().ToUpper();

                if (input.StartsWith("RAND", StringComparison.Ordinal))
                {
                    game.AddShipToRandomPos(currentShipType);
                    return;
                }

                try
                {
                    coordinates = new[] 
                    {
                        lettersIndexes[input[0]],
                        Convert.ToInt32(input.Substring(1, input.Length - 1))
                    };
                }
                catch
                {
                    operationIsOk = false;
                    continue;
                }

                Console.CursorLeft = offset;

                Console.Write("Please select how to align it (v/h) ");

                ConsoleKey key = default(ConsoleKey);

                while (key != ConsoleKey.V && key != ConsoleKey.H)
                {
                    key = Console.ReadKey(true).Key;
                }

                alignment = key == ConsoleKey.V ? Alignment.Vertical : Alignment.Horizontal;

                operationIsOk = game.TryAddShip(coordinates[0], coordinates[1], alignment, currentShipType);

            } while (!operationIsOk);
        }

        internal static void UpdateMaps(Game game)
        {
            Console.Clear();

            byte xoffset = 14;

            var letters = lettersIndexes.Keys.ToArray();

            for (int i = 1; i <= game.gameFieldSize; i++)
            {
                Console.SetCursorPosition(offset - 2 + xoffset, offset + i);
                Console.Write(i);

                Console.SetCursorPosition(offset - 2 + xoffset + 28, offset + i);
                Console.Write(i);

                Console.SetCursorPosition(offset + xoffset + i * 2 - 1, offset - 1);
                Console.Write(letters[i - 1]);

                Console.SetCursorPosition(offset + xoffset + 28 + i * 2 - 1, offset - 1);
                Console.Write(letters[i - 1]);
            }

            for (int x = 1; x <= game.gameFieldSize; x++)
            {
                for (int y = 1; y <= game.gameFieldSize; y++)
                {
                    bool wasUnderFire = game.Player1.shotsHistory.ContainsKey(Position.Get(x, y));
                    bool wasUnderOpponentsFire = game.Player2.shotsHistory.ContainsKey(Position.Get(x, y));
                    bool isShip = game.Player1.gameField.Fleet.ContainsKey(Position.Get(x, y));
                    bool isHit = isShip && wasUnderOpponentsFire;

                    Console.BackgroundColor = isHit ?
                        ConsoleColor.Red
                            : wasUnderOpponentsFire ?
                                ConsoleColor.DarkGray :
                                    isShip ?
                                    ConsoleColor.Green :
                                        ConsoleColor.Gray;

                    Console.SetCursorPosition(x * 2 - 1 + offset + xoffset, y + offset);

                    Console.Write(isHit ? "##" : "  ");

                    isHit =
                        game.Player2.gameField.Fleet.ContainsKey(Position.Get(x, y)) && wasUnderFire;

                    Console.BackgroundColor = isHit ?
                            ConsoleColor.Red
                                : wasUnderFire ?
                                    ConsoleColor.DarkGray :
                                        ConsoleColor.Gray;

                    Console.SetCursorPosition(x * 2 - 1 + 28 + offset + xoffset, y + offset);

                    Console.Write(isHit ? "##" : "  ");

                    Console.BackgroundColor = ConsoleColor.Black;
                }
            }

            Console.WriteLine("\n\n");
        }
    }
}
