﻿namespace GameOfBattleships.Core
{
    public struct Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Position(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Position Pos(int x, int y)
        {
            return new Position(x, y);
        }

        public override bool Equals(object obj)
        {
            return obj.GetHashCode() == GetHashCode();
        }

        public bool Equals(Position obj)
        {
            return obj.GetHashCode() == GetHashCode();
        }

        public override int GetHashCode()
        {
            return this.X * 1000 + this.Y * 10;
        }

        public static Position Shift(Position position, int x, int y)
        {
            position.X += x;
            position.Y += y;
            return position;
        }

        public static Position Get(int x, int y)
        {
            return new Position(x, y);
        }

        public static bool operator ==(Position p1, Position p2)
        {
            return p1.GetHashCode() == p2.GetHashCode();
        }

        public static bool operator !=(Position p1, Position p2)
        {
            return p1.GetHashCode() == p2.GetHashCode();
        }
    }
}
