﻿namespace GameOfBattleships.Core.Implementations
{
    sealed class BattleShip : AbstractShip
    {
        public new const byte length = 5;

        public BattleShip(Position startXY, Alignment alignment) 
            : base(startXY, alignment, length)
        {
        }

        public BattleShip(int x, int y, Alignment alignment, byte length) 
            : base(Position.Get(x, y), alignment, length)
        {
        }
    }
}
