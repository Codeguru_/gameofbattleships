﻿namespace GameOfBattleships.Core.Implementations
{
    sealed class Destroyer : AbstractShip
    {
        public new const byte length = 4;

        public Destroyer(Position startXY, Alignment alignment) 
            : base(startXY, alignment, length)
        {
        }

        public Destroyer(int x, int y, Alignment alignment, byte length) 
            : base(Position.Get(x, y), alignment, length)
        {
        }
    }
}
