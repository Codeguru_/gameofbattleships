﻿using System;
using System.Collections.Generic;

namespace GameOfBattleships.Core
{
    public class GameField
    {
        internal readonly byte _gameFieldSize;

        internal readonly Dictionary<Position, AbstractShip> _fleetPos;

        public IReadOnlyDictionary<Position, AbstractShip> Fleet { get { return _fleetPos; } }

        public GameField(byte gameFieldSize = 10)
        {
            _gameFieldSize = gameFieldSize;
            _fleetPos = new Dictionary<Position, AbstractShip>();
        }

        internal void AddShip(AbstractShip battleShip)
        {
            for (int i = 0; i < battleShip.length; i++)
            {
                switch (battleShip.alignment)
                {
                    case Alignment.Horizontal:
                        _fleetPos.Add(Position.Shift(battleShip.position, i, 0), battleShip);
                        break;
                    case Alignment.Vertical:
                        _fleetPos.Add(Position.Shift(battleShip.position, 0, i), battleShip);
                        break;
                }
            }
        }

        internal bool CheckIfPosIsOk(int x, int y, Alignment alignment, int shipsLength)
        {
            if (alignment == Alignment.Horizontal &&
                  x + shipsLength > _gameFieldSize) return false;

            if (alignment == Alignment.Vertical &&
                  y + shipsLength > _gameFieldSize) return false;

            for (int i = 0; i < shipsLength; i++)
            {
                switch (alignment)
                {
                    case Alignment.Horizontal:
                        if (_fleetPos.ContainsKey(Position.Get(x + i, y))) return false;
                        break;
                    case Alignment.Vertical:
                        if (_fleetPos.ContainsKey(Position.Get(x, y + i))) return false;
                        break;
                }
            }

            return true;
        }
    }
}
