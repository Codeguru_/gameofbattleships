﻿using GameOfBattleships.Core.Implementations;
using System;
using System.Linq;
using System.Reflection;

namespace GameOfBattleships.Core
{
    public sealed partial class GameOfBattleships
    {
        /// <summary>
        /// Actions - player moves, and gameplay actions
        /// </summary>

        public void AddShipToRandomPos(Type shipType)
        {
            var rand = new Random();

            while (!TryAddShip(rand.Next(1, 11), rand.Next(1, 11), (Alignment)rand.Next(0, 2), shipType)) { }
        }

        public bool TryAddDestroyer(int x, int y, Alignment alignment)
        {
            return TryAddShip(x, y, alignment, typeof(Destroyer));
        }

        public bool TryAddBattleship(int x, int y, Alignment alignment)
        {
            return TryAddShip(x, y, alignment, typeof(BattleShip));
        }

        public bool TryAddShip(int x, int y, Alignment alignment, Type shipType)
        {
            if(!PlayerToMove.availableShips.ContainsKey(shipType) || 
                PlayerToMove.availableShips[shipType] < 1)
            {
                return false;
            }

            var shipsLength = (byte)shipType.GetFields().First(f => f.Name == "length").GetRawConstantValue();

            if (!PlayerToMove.gameField.CheckIfPosIsOk(x, y, alignment, shipsLength))
            {
                return false;
            }

            var ship = Activator.CreateInstance(shipType,
                Position.Get(x, y), alignment) as AbstractShip;

            if(ship == null)
            {
                throw new InvalidOperationException("Unable to create ship of type " + shipType.Name);
            }

            PlayerToMove.gameField.AddShip(ship);
            PlayerToMove.availableShips[shipType]--;

            _pl1ToMove = !_pl1ToMove;

            return true;
        }

        public bool Shoot(Position position)
        {
            AbstractShip ship = null;
            bool result = false;

            if (Opponent.HasShipOnPos(position))
            {
                ship = Opponent.gameField.Fleet[position];
                Opponent.MarkAsHit(position);
                result = true;
            }

            if (!PlayerToMove.shotsHistory.ContainsKey(position))
            {
                PlayerToMove.shotsHistory.Add(position, result);

                if (ship != null && !Opponent.gameField._fleetPos.Values.Any(v => v == ship))
                {
                    Opponent.latestDestroyedShip = ship;
                }
            }
            else
            {
                Opponent.latestDestroyedShip = null;
            }

            if (!result)
            {
                _pl1ToMove = !_pl1ToMove;
            }

            return result;
        }
    }
}
