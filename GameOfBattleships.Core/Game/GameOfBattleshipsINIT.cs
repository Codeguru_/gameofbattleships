﻿using GameOfBattleships.Core.Abstractions;
using GameOfBattleships.Core.AIPlayer;
using GameOfBattleships.Core.Implementations;
using System;
using System.Collections.Generic;

namespace GameOfBattleships.Core
{
    public sealed partial class GameOfBattleships
    {
        /// <summary>
        /// Initialization - declarations, ctors, etc
        /// </summary>

        private Player _player1;
        private Player _player2;
        private AbstractAIPlayer _aiPlayer;
        public readonly byte gameFieldSize;
        private bool gameIsInitialized;
        private bool _pl1ToMove;
        private bool _vsAIPlayer;

        public bool VsAIPlayer { get { return _vsAIPlayer; } }
        public bool Pl1ToMove { get { return _pl1ToMove; } }
        public AbstractAIPlayer AiPlayer { get { return _aiPlayer; } }
        public Player Player1 { get { return _player1; } }
        public Player Player2 { get { return _player2; } }
        public Player PlayerToMove { get { return _pl1ToMove ? _player1 : _player2; } }
        public Player Opponent { get { return _pl1ToMove ? _player2 : _player1; } }

        public Dictionary<Type, int> DefaultShipsSet
        {
            get
            {
                return new Dictionary<Type, int>()
                {
                    { typeof(BattleShip), 1 },
                    { typeof(Destroyer), 2 }
                };
            }
        }

        private GameOfBattleships() { }

        public GameOfBattleships(byte fieldSize = 10)
        {
            this.gameFieldSize = fieldSize;
        }

        public void StartNewGame(string pl1Name = "player1", string pl2Name = "player2", bool vsAIPlayer = true, AbstractAIPlayer aiPlayer = null)
        {
            if (gameIsInitialized) return;

            var pl1GameField = new GameField(gameFieldSize);
            var pl2GameField = new GameField(gameFieldSize);

            var pl1AvailableShipsToPlace = DefaultShipsSet;
            var pl2AvailableShipsToPlace = DefaultShipsSet;

            _player1 = new Player(pl1GameField, DefaultShipsSet, pl1Name);
            _player2 = new Player(pl2GameField, DefaultShipsSet, pl2Name);
            _vsAIPlayer = vsAIPlayer;
            _aiPlayer = vsAIPlayer ? aiPlayer == null ? new SimpleAIPlayer(_player2) : null : aiPlayer;

            // Randomizes which player starts the game
            _pl1ToMove = DateTime.Now.Second % 2 == 0;

            gameIsInitialized = true;
        }
    }
}
