﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameOfBattleships.Core
{
    public sealed class Player
    {
        public readonly string name;
        public readonly GameField gameField;
        public readonly IDictionary<Position, bool> shotsHistory;
        public readonly IDictionary<Type, int> availableShips;
        public AbstractShip latestDestroyedShip;

        public bool FleetIsDestroyed { get { return gameField.Fleet.Values.All(square => square == null); } }

        private Player() { }

        public Player(GameField gameField, Dictionary<Type, int> availableShips, string name = "Unnamed Player")
        {
            this.name = name;
            this.gameField = gameField;
            this.shotsHistory = new Dictionary<Position, bool>();
            this.availableShips = availableShips;
            this.latestDestroyedShip = null;
        }

        internal bool HasShipOnPos(Position position)
        {
            return this.gameField._fleetPos.ContainsKey(position);
        }

        internal bool HasShipOnPos(int x, int y)
        {
            return HasShipOnPos(Position.Get(x, y));
        }

        internal void MarkAsHit(Position position)
        {
            this.gameField._fleetPos[position] = null;
        }

        internal void MarkAsHit(int x, int y)
        {
            MarkAsHit(Position.Get(x, y));
        }

        public bool IsShipDestroyed(Position pos)
        {
            var result = latestDestroyedShip != null && 
                latestDestroyedShip.GetCoordinates().Any(c => c == pos);

            return result;
        }
    }
}