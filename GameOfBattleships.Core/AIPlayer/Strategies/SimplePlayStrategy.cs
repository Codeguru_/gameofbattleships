﻿using System;
using System.Collections.Generic;
using GameOfBattleships.Core.Abstractions;
using System.Linq;

namespace GameOfBattleships.Core.AIPlayer
{
    public class SimplePlayStrategy : IPlayStrategy
    {
        readonly Random rand = new Random();

        public Position SelectTarget(IDictionary<Position, bool> shotsHistory, int gameFieldSize)
        {
            var result = default(Position);

            if (shotsHistory.Any(sh => sh.Value))
            {
                var lastHit = shotsHistory.Last(sh => sh.Value);

                Position prevLastHitPos = shotsHistory.FirstOrDefault(sh =>
                    sh.Value &&
                    Math.Abs(sh.Key.X - lastHit.Key.X) + Math.Abs(sh.Key.Y - lastHit.Key.Y) == 1).Key;

                if(prevLastHitPos.X != 0 && prevLastHitPos.Y != 0)
                {
                    var newPos = Position.Shift(
                        lastHit.Key, lastHit.Key.X - prevLastHitPos.X,
                        lastHit.Key.Y - prevLastHitPos.Y);

                    if(!shotsHistory.ContainsKey(newPos) &&
                        newPos.X > 1 && newPos.X < gameFieldSize &&
                        newPos.Y > 1 && newPos.Y < gameFieldSize)
                    {
                        return newPos;
                    }

                    newPos = Position.Shift(
                        lastHit.Key, prevLastHitPos.X - lastHit.Key.X,
                        prevLastHitPos.Y - lastHit.Key.Y);
                     
                    if(!shotsHistory.ContainsKey(newPos) && 
                        newPos.X > 1 && newPos.X < gameFieldSize &&
                        newPos.Y > 1 && newPos.Y < gameFieldSize)
                    {
                        return newPos;
                    }
                }

                for (int i = -1; i < 2; i += 2)
                {
                    Position newPos = Position.Shift(lastHit.Key, 0, i);

                    if (newPos.Y > 1 && newPos.Y < gameFieldSize && !shotsHistory.ContainsKey(newPos))
                    {
                        return newPos;
                    }

                    newPos = Position.Shift(lastHit.Key, i, 0);

                    if (newPos.X > 1 && newPos.X < gameFieldSize && !shotsHistory.ContainsKey(newPos))
                    {
                        return newPos;
                    }
                }
            }

            do
            {
                result = Position.Get(rand.Next(1, gameFieldSize + 1), rand.Next(1, gameFieldSize + 1));
            } while (shotsHistory.ContainsKey(result));

            return result;
        }
    }
}