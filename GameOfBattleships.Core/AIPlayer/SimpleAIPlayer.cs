﻿using GameOfBattleships.Core.Abstractions;

namespace GameOfBattleships.Core.AIPlayer
{
    public class SimpleAIPlayer : AbstractAIPlayer
    {
        readonly Player _player;

        public SimpleAIPlayer(Player player) : base(player, new SimplePlayStrategy())
        {
            _player = player;
        }
    }
}
