﻿namespace GameOfBattleships.Core.Abstractions
{
    public abstract class AbstractAIPlayer
    {
        readonly Player _player;
        readonly IPlayStrategy _strategy;

        protected AbstractAIPlayer(Player player, IPlayStrategy strategy)
        {
            _player = player;
            _strategy = strategy;
        }

        public virtual Position MakeTurn()
        {
            return _strategy.SelectTarget(_player.shotsHistory, _player.gameField._gameFieldSize);
        }
    }
}
