﻿using System.Collections.Generic;

namespace GameOfBattleships.Core.Abstractions
{
    public interface IPlayStrategy
    {
        Position SelectTarget(IDictionary<Position, bool> shotsHistory, int gameFieldSize);
    }
}
