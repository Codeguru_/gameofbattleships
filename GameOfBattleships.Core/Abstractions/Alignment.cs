﻿namespace GameOfBattleships.Core
{
    public enum Alignment
    {
        Horizontal = 0,
        Vertical = 1
    }
}