﻿using System.Linq;

namespace GameOfBattleships.Core
{
    public abstract class AbstractShip
    {
        public readonly string name;
        public readonly Position position;
        public readonly Alignment alignment;
        public readonly byte length;
        
        private AbstractShip() { }

        protected internal AbstractShip(Position position, Alignment alignment, byte length)
        {
            this.alignment = alignment;
            this.length = length;
            this.position = position;
            this.name = this.GetType().Name;
        }

        internal Position[] GetCoordinates()
        {
            var result = new Position[length];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] =
                    alignment == Alignment.Horizontal ?
                      Position.Shift(position, i, 0) :
                          Position.Shift(position, 0, i);
            }

            return result;
        }
    }
}